package automationFramework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class CartPage {
	
	private final WebDriver driver;

	 By continueShoppingLocator = By.id("cartContinueShopping");
	 By subTotalLocator = By.xpath("//*[@id=\"subtotal\"]/table/tbody/tr[1]/td[3]/div");
	 By closeButtonLocator = By.cssSelector("a[class='nyroModalClose nyroModalCloseButton nmReposition']");
	 	    
	public CartPage(WebDriver driver) {
      this.driver = driver;

      // Check that we're on the right page.
      if (!(driver.findElement(continueShoppingLocator).isDisplayed())) {

      	throw new IllegalStateException("This is not the cart page!");
      }
    }
	
	public String getProductTitle(By productTitle) {
	       
	    return driver.findElement(productTitle).getText();    
	}
	   
	public String getProductPrice(By productPrice) {
	       
	   return driver.findElement(productPrice).getText();    
	}
	
	 public CartPage closePopup() {
	    driver.findElement(closeButtonLocator).click();

	    return this;    
	 }
	 
	 public CartPage selectCombobox(By selector, String value) {
		 Select oSelection1 = new Select(driver.findElement(selector));
		 
		 oSelection1.selectByVisibleText(value);

		 return this;    
	 }
	
	 public String getSubTotal() {
		       
		 return driver.findElement(subTotalLocator).getText();    
	 }
	 
	 public CartPage clickAddOther() {
		 driver.findElement(continueShoppingLocator).click();

		 return this;    
	 }
}
