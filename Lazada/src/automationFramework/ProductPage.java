package automationFramework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProductPage {

	private final WebDriver driver;

	 By itemTitleLocator = By.id("prod_title");
	 By itemPriceLocator = By.id("special_price_box");
	 By addCartButtonLocator = By.cssSelector("span[class='submit_btn_text']");
	 	    
	public ProductPage(WebDriver driver) {
       this.driver = driver;

       // Check that we're on the right page.
       if (!(driver.findElement(addCartButtonLocator).isDisplayed())) {

       	throw new IllegalStateException("This is not the product page!");
       }
   }
   
   public String getItemTitle() {
       
       return driver.findElement(itemTitleLocator).getText();    
   }
   
   public String getItemPrice() {
       
       return driver.findElement(itemPriceLocator).getText();    
   }
   
   public ProductPage clickAddCart() {
       driver.findElement(addCartButtonLocator).click();

       return this;    
   }
   
}
