package automationFramework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class SearchPage {

	private final WebDriver driver;
	
	By firstItemLocator = By.xpath("//*[@class=\"catalog__main__content\"]/div[2]/a[1]");
	By searchBoxLocator = By.id("searchInput");
	///html/body/div[2]/div[2]/div[2]/div/div[2]/a[1]
	
	public SearchPage(WebDriver driver) {
	       this.driver = driver;

	       // Check that we're on the right page.
	       if (!(driver.findElement(firstItemLocator).isDisplayed())) {

	       	throw new IllegalStateException("This is not the search page!");
	       }
	}
	
	public SearchPage clickItem() {
		new Actions(driver).moveToElement(driver.findElement(firstItemLocator)).perform();
	    driver.findElement(firstItemLocator).click();

	    return this;    
	}
	
	public SearchPage clickSearchBox() {
        driver.findElement(searchBoxLocator).click();
        
        return this;    
    }
}
