package automationFramework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {

	private final WebDriver driver;

	public HomePage(WebDriver driver) {
        this.driver = driver;

        // Check that we're on the right page.
        if (!"Mua hàng Trực tuyến Giá rẻ - Mua hàng trên Lazada Việt Nam".equals(driver.getTitle())) {
            throw new IllegalStateException("This is not the home page!");
        }
    }
    
    By registerLocator = By.xpath("/html/body/div[1]/div[1]/div/ul/li[5]/span");
    By searchBoxLocator = By.id("searchInput");
    By searchButtonLocator = By.className("header__search__submit");
    
    public HomePage clickRegister() {
         driver.findElement(registerLocator).click();

         return this;    
    }
    
    public HomePage typeSearchBox(String product) {
        driver.findElement(searchBoxLocator).sendKeys(product);
        
        return this;    
    }
    
    public HomePage clickSearch() {
        driver.findElement(searchButtonLocator).click();

        return this;    
    }
    
    public HomePage addItemToCart(String productName) throws InterruptedException {
        typeSearchBox(productName);
        Thread.sleep(1000);
        clickSearch();
        Thread.sleep(2000);
        SearchPage sp = new SearchPage(driver);
	    sp.clickItem();
	    Thread.sleep(3000);
	    ProductPage pp = new ProductPage(driver);
	    pp.clickAddCart();
	    Thread.sleep(2000);
	    return this;
    }
}
