package automationFramework;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;



public class TestSupport {
	
	private WebDriver driver = null;
	
	public TestSupport() {
        //this.driver = obj;
    }
	
	public WebDriver getBrowserDriver( )
	{
		String browser = TestSupport.readFileText("D:/Test_Config/browser_test.txt");
		
		if (browser.equals("FF")){
			driver = new FirefoxDriver();
		}
		else if(browser.equals("IE")){
			System.setProperty("webdriver.ie.driver", "D:/Test_Config/IEDriverServer.exe");
	        driver=new InternetExplorerDriver();
		}
		else if(browser.equals("Chrome")){
			System.setProperty("webdriver.chrome.driver", "D:/Test_Config/chromedriver.exe");
			driver = new ChromeDriver();
		}
		else {
			System.out.println("You have configed incorrect browser information.");                      
		}
		
		// Maximize the window
		driver.manage().window().maximize();
		
		//Put a Implicit wait, this means that any search for elements on the page could take the time the implicit wait is set for before throwing exception
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		return driver;
	}
	
	public void closeBrowserDriver() throws IOException, InterruptedException{
		// Get the information which browser is using
		String browser = TestSupport.readFileText("D:/Test_Config/browser_test.txt");
		
		if (browser.equals("FF")){
			Runtime.getRuntime().exec("taskkill /F /IM firefox.exe");
		    Thread.sleep(5000);
		    Runtime.getRuntime().exec("taskkill /F /IM plugin-container.exe");
		    Runtime.getRuntime().exec("taskkill /F /IM WerFault.exe");
		}
		else if(browser.equals("IE")){
			driver.quit();
		}
		else if(browser.equals("Chrome")){
			driver.quit();
		}
	}

	public static Integer removeChar(String str){
		Integer result;
		str = str.replaceAll("[^0-9]","");
		result = Integer.parseInt(str);
		return result;
	}
	
	public static String readFileText(String fileName) 
    {

       String txt_result = new String();
       
       try{

          //Create object of FileReader
          FileReader inputFile = new FileReader(fileName);

          //Instantiate the BufferedReader Class
          BufferedReader bufferReader = new BufferedReader(inputFile);

          //Variable to hold the one line data
          String line;

          // Read file line by line and print on the console
          while ((line = bufferReader.readLine()) != null)   {
            txt_result = line;
          }
          //Close the buffer reader
          bufferReader.close();
       }catch(Exception e){
          System.out.println("Error while reading file line by line:" + e.getMessage());                      
       }

       return txt_result;
    		   
     }

}
