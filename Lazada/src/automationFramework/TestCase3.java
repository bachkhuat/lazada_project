package automationFramework;

import static org.junit.Assert.*;

import java.io.IOException;

import org.openqa.selenium.*;
import org.junit.Test;

public class TestCase3 {

	By product1PriceLocator = By.xpath("//*[@id=\"cart-items-list-form\"]/div[2]/table/tbody/tr/td[3]/span[1]");
	By product2PriceLocator = By.xpath("//*[@id=\"cart-items-list-form\"]/div[2]/table[2]/tbody/tr/td[3]/span[1]");
	By product3PriceLocator = By.xpath("//*[@id=\"cart-items-list-form\"]/div[2]/table[3]/tbody/tr/td[3]/span[1]");
	By combobox1Locator = By.xpath("//*[@id=\"cart-items-list-form\"]/div[2]/table/tbody/tr/td[4]/select");
	By combobox3Locator = By.xpath("//*[@id=\"cart-items-list-form\"]/div[2]/table[3]/tbody/tr/td[4]/select");
	
	@Test
	public void test() throws IOException, InterruptedException {
		
		// Get the browser driver
		TestSupport ts = new TestSupport();
		WebDriver driver = ts.getBrowserDriver();
				
		//Launch the Website
	    driver.get("http://www.lazada.vn");

	    // Search with text "iPhone 6 Plus" and add the first result item into cart
	    HomePage hp = new HomePage(driver);
	    hp.addItemToCart("iPhone 6 Plus");

	    //Get the price of the product
	    CartPage cp = new CartPage(driver);
	    String str_price1 = cp.getProductPrice(product1PriceLocator);
	    
	    // Change the quantity of product to "2"
	    cp.selectCombobox(combobox1Locator, "2");
	    
	    Thread.sleep(2000);
	    
	    //Get the sub total of the bill
	    String str_subtotal = cp.getSubTotal();
	    
	    // Convert the price of the product to integer and calculate
	    Integer price1 = TestSupport.removeChar(str_price1);
	    Integer sum_price = price1 * 2;
	    Integer sub_total = TestSupport.removeChar(str_subtotal);
	    
	    // VP: Check the sub total
	    assertTrue(sum_price.equals(sub_total));
	    
	    // Click on the link Add another product
	    cp.clickAddOther();
	    
	    // Search with text "Tivi LED LG" and add the first result item into cart
	    hp.addItemToCart("Tivi LED LG");
	    
	    // Click on the link Add another product
	    cp.clickAddOther();
	    
	    // Search with text "May anh KTS" and add the first result item into cart
	    hp.addItemToCart("May anh KTS");
	    
	    // Get the price of the second product
	    String str_price2 = cp.getProductPrice(product2PriceLocator);
		
	    // Get the price of the third product
	    String str_price3 = cp.getProductPrice(product3PriceLocator);
		
	    // Get the sub total of the bill
	    str_subtotal = cp.getSubTotal();
	    
	    // Convert the price of the product to integer and calculate
	    Integer price2 = TestSupport.removeChar(str_price2);
	    Integer price3 = TestSupport.removeChar(str_price3);
	    
	    sum_price = price1 * 2 + price2 + price3;
	    sub_total = TestSupport.removeChar(str_subtotal);
	    
	    // VP: Check the sub total
	    assertTrue(sum_price.equals(sub_total));
	    
	    // Change the quantity of the third product to "3"
	    cp.selectCombobox(combobox3Locator, "3");
	    
	    Thread.sleep(2000);
	    
	    // Get the sub total of the bill
	    str_subtotal = cp.getSubTotal();
	    
	    // Convert the price of the product to integer and calculate
	    sub_total = TestSupport.removeChar(str_subtotal);
	    sum_price = price1 * 2 + price2 + price3 * 3;
	    
	    // VP: Check the sub total
	    assertTrue(sum_price.equals(sub_total));
	    
	    // Click on the button "X" to close the popup dialog
	    cp.closePopup();
	    
	    // Close the driver
	    ts.closeBrowserDriver();
	    
	}

}

