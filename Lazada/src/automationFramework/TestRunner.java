package automationFramework;


import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {

	public static void main(String[] args) {

			Result result1 = JUnitCore.runClasses(TestCase1.class);
		    for (Failure failure : result1.getFailures()) {
		       System.out.println(failure.toString());
		    }
		    System.out.println("Test case 1: " + result1.wasSuccessful());
		    
		    Result result2 = JUnitCore.runClasses(TestCase2.class);
		    for (Failure failure : result2.getFailures()) {
		       System.out.println(failure.toString());
		    }
		    System.out.println("Test case 2: " + result2.wasSuccessful());
		    Result result3 = JUnitCore.runClasses(TestCase3.class);
		    for (Failure failure : result3.getFailures()) {
		       System.out.println(failure.toString());
		    }
		    System.out.println("Test case 3: " + result3.wasSuccessful());
		
		  
	}
}
