package automationFramework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegisterPage {

	private final WebDriver driver;

	 By sendButtonLocator = By.id("send");
	 By usernameLocator = By.id("RegistrationForm_first_name");
	 By emailLocator = By.id("RegistrationForm_email");
	 By passwordLocator = By.id("RegistrationForm_password");
	 By confirmPasswordLocator = By.id("RegistrationForm_password2");
	 By closeButtonLocator = By.cssSelector("a[class='nyroModalClose nyroModalCloseButton nmReposition']");
	 By errorMessageLocator = By.cssSelector("span[class='error-password s-error msg error-display']");
	    
	public RegisterPage(WebDriver driver) {
        this.driver = driver;

        // Check that we're on the right page.
        if (!(driver.findElement(usernameLocator).isDisplayed())) {

        	throw new IllegalStateException("The register popup has not displayed yet!");
        }
    }
    
    public RegisterPage typeUsername(String username) {
        driver.findElement(usernameLocator).sendKeys(username);

        return this;    
    }
    
    public RegisterPage typePassword(String password) {
        driver.findElement(passwordLocator).sendKeys(password);

        return this;    
    }
    
    public RegisterPage typeConfirmPassword(String cfpassword) {
        driver.findElement(confirmPasswordLocator).sendKeys(cfpassword);

        return this;    
    }
    
    public RegisterPage typeEmail(String email) {
        driver.findElement(emailLocator).sendKeys(email);

        return this;    
    }
    
    public RegisterPage submitRegister() {

    	driver.findElement(sendButtonLocator).click();

    	return this;    
    }
    
    public String getErrorMessage() {
        String message = driver.findElement(errorMessageLocator).getText();

        return message;    
    }
    
    public RegisterPage closePopup() {
        driver.findElement(closeButtonLocator).click();

        return this;    
    }
    
    public RegisterPage registerAs(String username, String email, String password, String cfpassword) {
        typeUsername(username);
        typeEmail(email);
        typePassword(password);
        typeConfirmPassword(cfpassword);
        return submitRegister();
    }
    
}
