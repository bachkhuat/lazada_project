package automationFramework;

import static org.junit.Assert.*;

import java.io.IOException;

import org.openqa.selenium.*;
import org.junit.Test;


public class TestCase1 {

	@Test
	public void test() throws InterruptedException, IOException {
			
		// Get the browser driver
		TestSupport ts = new TestSupport();
		WebDriver driver = ts.getBrowserDriver();
		
	    //Launch the Website
		driver.get("http://www.lazada.vn");
		
	    HomePage hp = new HomePage(driver);

	    // Click on the link "Dang ky"
	    hp.clickRegister();
	    
	    Thread.sleep(2000);
	    
	    RegisterPage rp = new RegisterPage(driver);
	    
	    //Input user name, email, and password without confirm-password 
	    rp.registerAs("testuser", "Test@123.abc", "abc123","");

	    String checkMessage = new String("Mật khẩu phải có ít nhất 1 chữ số");
	    
	    // VP: Check the message
	    assertTrue(rp.getErrorMessage().equals(checkMessage)); 
	    
	    // Close the popup dialog
	    rp.closePopup();

	    // Close the driver
	    ts.closeBrowserDriver();
	    
	}

}

