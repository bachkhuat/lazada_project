package automationFramework;

import static org.junit.Assert.*;

import java.io.IOException;
import org.openqa.selenium.*;
import org.junit.Test;

public class TestCase2 {

	By productTitleLocator = By.cssSelector("div[class='productdescription']");
	By productPriceLocator = By.xpath("//*[@id=\"cart-items-list-form\"]/div[2]/table/tbody/tr/td[3]/span[1]");
	
	@Test
	public void test() throws IOException, InterruptedException {
		
		// Get the browser driver
		TestSupport ts = new TestSupport();
		WebDriver driver = ts.getBrowserDriver();
				
	    //Launch the Website
	    driver.get("http://www.lazada.vn");

	    // Enter the text "iPhone 6" into the Search box
	    HomePage hp = new HomePage(driver);
	    hp.typeSearchBox("iPhone 6");
	    
	    Thread.sleep(1000);
	    
	    // Click the button "Tim Kiem" 
	    hp.clickSearch();

	    Thread.sleep(1000);
	    
	    // Click on the first resulted item 
	    SearchPage sp = new SearchPage(driver);
	    sp.clickItem();
	    
	    Thread.sleep(2000);
	    
	    // Get the title and the price of the product in product page
	    ProductPage pp = new ProductPage(driver);
	    String pdp_title = pp.getItemTitle();
	    String pdp_price = pp.getItemPrice();
	    
	    // Click on the button "Mua ngay"
	    pp.clickAddCart();
	    
	    Thread.sleep(2000);
	    
	    // Get the title and the price of the product in cart page 
	    CartPage cp = new CartPage(driver);
	    String cp_title = cp.getProductTitle(productTitleLocator);
	    String cp_price = cp.getProductPrice(productPriceLocator);
	    
	    // VP: check the title and the price
	    assertTrue(pdp_title.equals(cp_title));
	    assertTrue(pdp_price.equals(cp_price));
	    
	    // Click on the button "X" to close the popup dialog
	    cp.closePopup();

	    // Close the driver
	    ts.closeBrowserDriver();
	    
	   
	}

}
