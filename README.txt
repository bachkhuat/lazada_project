﻿Hello!
Here is my test exercise at Lazada.

====================
Configuration to run the project:

1. Clone the project from Bitbucket with the git command line: 
	"git clone https://bachkhuat@bitbucket.org/bachkhuat/lazada_project.git"
2. Import the project Lazada into Eclipse
	2.1. Config Java Compile is Java-SE 1.6 or 1.7
	2.2. Extract the rar file "selenium-java-2.44.0" 
	2.3. Add the Selenium libraries from file have been extracted
3. Copy folder "Test_Config" to your driver D
====================

====================
Run the test:

1. You can run the test with 1 of 3 browser: Firefox, Internet Explorer and Chrome by edit the file "D:\Test_Config\browser_test.txt" as below:
	- FF: run with Firefox (successfull with FF ver. 34)
	- IE: run with Internet Explorer (successfull with IE ver. 10)
	- Chrome: run with Chrome (successfull with Chrome ver. 39)
2. In Eclipse, you run the file "TestRunner.java" to run all 3 test cases or you can run every test case with 'TestCase1.java", "TestCase2.java", "TestCase3.java"
====================

Contact me:
If you get the stuck or problem when run the project, please contact me via:
	- Skype: duy_bach_89 
	- Email: duybach_1989@yahoo.com.vn
	- Phone: 0902683265
Thank you!